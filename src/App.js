import React from "react";
import "./App.scss";
import Numpad from "./components/Numpad";
import Message from "./components/Message";

const App = () => {
  return (
    <div className="App">
      <Numpad />
      <Message />
    </div>
  );
};

export default App;
