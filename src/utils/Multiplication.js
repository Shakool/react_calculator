export class Multiplication {
  constructor(rounds = 5) {
    this.currentRound = 0;
    this.maxRounds = rounds;
    this.generateNewMultiplication();
  }

  generateNewMultiplication() {
    this.currentRound++;

    this.left = Math.floor(Math.random() * 15 + 1);
    this.right = Math.floor(Math.random() * 15 + 1);
  }

  getCurrentResult() {
    return this.left * this.right;
  }
  getPrintableChallenge() {
    return `${this.left}x${this.right}`;
  }
}
