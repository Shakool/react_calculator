import { SEND_NUMBER, VALIDATE_INPUT, CANCEL_INPUT, RESET } from "../constants/action-type";
import { Multiplication } from "../utils/Multiplication";

const stateInit = {
  challenge: new Multiplication(),
  number: "",
  result: 0,
  score: 0,
  isOver: false,
};

export default (state = stateInit, action = {}) => {
  let userInput = "";
  let result = 0;
  let score = state.score + 0;
  let isOver = false;
  switch (action.type) {
    case SEND_NUMBER:
      console.log(`Number Sent  :  ${action.payload}`);
      userInput = `${state.number}${action.payload}`;
      return { ...state, number: userInput };
    case VALIDATE_INPUT:
      result = state.challenge.getCurrentResult();
      console.log(`Input Validated. Result is ${result}`);

      if (state.number !== "") {
        result = result === parseInt(state.number);
        console.log(`Comparison with user input : ${result}`);

        console.log(`Resetting the board...`);
        score = result ? score + 1 : score;
        userInput = "";
        state.challenge.generateNewMultiplication();

        console.log(`Is Game Over ? ${state.isOver}`);
        isOver = state.challenge.currentRound >= state.challenge.maxRounds ? true : false;

        return { ...state, result: result, number: userInput, score: score, isOver: isOver };
      }
    case CANCEL_INPUT:
      console.log(`Input Cancelled `);
      userInput = "";
      return { ...state, number: userInput };
    case RESET:
      console.log(`Game Reset`);
      return { ...stateInit, challenge: new Multiplication() };
    default:
      return state;
  }
};

console.log(stateInit.challenge);
