import React from "react";
import { connect } from "react-redux";
import Key from "./Key";
import "./Message.scss";

const Message = (props) => {
  console.log(props);
  const { userValue, challenge, result, score, isOver } = props;
  const resultDiv =
    result === 0 ? (
      <div className="result-box result-hidden"></div>
    ) : result ? (
      <div className="result-box result-success">GG</div>
    ) : (
      <div className="result-box result-failure">You failed at life</div>
    );
  const scoreDiv = isOver ? (
    <small>
      Game Over. Score final : {score} <Key keyValue="Reset" size="auto" />
    </small>
  ) : (
    <small>Score : {score}</small>
  );

  return (
    <div className="Message">
      {/* Challenge */}
      <div className="challenge">&#10148; {challenge.getPrintableChallenge()}</div>

      {/* Input */}
      <span>Input : {userValue}</span>
      {/* Output */}
      <hr />
      {resultDiv}
      {scoreDiv}
    </div>
  );
};

const mapStateToProps = (state) => {
  return { userValue: state.number, challenge: state.challenge, result: state.result, score: state.score, isOver: state.isOver };
};

export default connect(mapStateToProps)(Message);
