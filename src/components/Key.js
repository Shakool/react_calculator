import React from "react";
import { connect } from "react-redux";
import { sendNumber, validateInput, cancelInput, reset } from "../actions/actions";
import "./Key.scss";

const Key = (props) => {
  const { keyValue, size, sendNumber, validateInput, cancelInput, reset, isOver } = props;
  let keyAction;

  if (keyValue === "✔") {
    keyAction = validateInput;
  } else if (keyValue === "✖") {
    keyAction = cancelInput;
  } else if (keyValue === "Reset") {
    keyAction = reset;
  }

  let classes = size !== undefined ? "Key double" : "Key";
  return (
    <button
      className={classes}
      onClick={!isNaN(parseInt(keyValue)) ? () => sendNumber(keyValue) : keyAction}
      disabled={isOver && keyValue !== "Reset"}
    >
      {keyValue}
    </button>
  );
};

// lancer les actions
const mapDispatchToProps = { sendNumber, validateInput, cancelInput, reset };

const mapStateToProps = (state) => {
  return { isOver: state.isOver };
};

export default connect(mapStateToProps, mapDispatchToProps)(Key);
