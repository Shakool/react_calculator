import React from "react";
import Key from "./Key";
import "./Numpad.scss";

const Numpad = () => {
  return (
    <div className="Numpad_container">
      <div className="Numpad">
        <Key keyValue="1" />
        <Key keyValue="2" />
        <Key keyValue="3" />
        <Key keyValue="4" />
        <Key keyValue="5" />
        <Key keyValue="6" />
        <Key keyValue="7" />
        <Key keyValue="8" />
        <Key keyValue="9" />
        <Key keyValue="0" />
      </div>
      <div className="validation">
        <Key keyValue="&#10004;" size="auto" />
        <Key keyValue="&#10006;" size="auto" />
      </div>
    </div>
  );
};

export default Numpad;
