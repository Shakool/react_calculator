import {
  SEND_NUMBER,
  VALIDATE_INPUT,
  CANCEL_INPUT,
  RESET,
  /* d'autres constantes d'actions ...*/
} from "../constants/action-type";

export const sendNumber = (payload) => {
  return {
    type: SEND_NUMBER,
    payload,
  };
};

export const validateInput = (payload) => {
  return {
    type: VALIDATE_INPUT,
    payload,
  };
};

export const cancelInput = (payload) => {
  return {
    type: CANCEL_INPUT,
    payload,
  };
};

export const reset = (payload) => {
  return {
    type: RESET,
    payload,
  };
};
